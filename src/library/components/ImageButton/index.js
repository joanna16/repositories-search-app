import React from 'react'
import PropTypes from 'prop-types'
import {
    TouchableOpacity, StyleSheet, Image
} from 'react-native'

export default function ImageButton(props) {
    const { onPress, image } = props

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.6}
            style={styles.touchableOpacity}
        >
            <Image source={image} />
        </TouchableOpacity>
    )
}

ImageButton.propTypes = {
    onPress: PropTypes.func.isRequired,
    image: PropTypes.number.isRequired
}

const styles = StyleSheet.create({
    touchableOpacity: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 3
    }
})
