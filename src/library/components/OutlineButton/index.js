import React from 'react'
import PropTypes from 'prop-types'
import {
    View, Text, TouchableOpacity, StyleSheet
} from 'react-native'
import colors from 'resources/colors'

export default function Button(props) {
    const {
        children,
        onPress,
        active,
        withoutBorderLeft,
        withoutBorderRight
    } = props

    return (
        <View style={[
            styles.view,
            active && styles.activeView,
            withoutBorderRight && styles.withoutBorderRight,
            withoutBorderLeft && styles.withoutBorderLeft
        ]}
        >
            <TouchableOpacity onPress={onPress} activeOpacity={1}>
                <Text style={[styles.text, active && styles.activeText]}>
                    {children}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

Button.defaultProps = {
    active: false,
    withoutBorderLeft: false,
    withoutBorderRight: false
}

Button.propTypes = {
    children: PropTypes.node.isRequired,
    onPress: PropTypes.func.isRequired,
    active: PropTypes.bool,
    withoutBorderLeft: PropTypes.bool,
    withoutBorderRight: PropTypes.bool
}

const styles = StyleSheet.create({
    view: {
        width: 105,
        borderWidth: 1,
        borderColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 8
    },
    activeView: {
        backgroundColor: colors.white
    },
    text: {
        fontSize: 15,
        color: colors.white,
        paddingTop: 8,
        paddingBottom: 8,
        textAlignVertical: 'center',
        flexWrap: 'nowrap'
    },
    activeText: {
        color: colors.darkGray
    },
    withoutBorderRight: {
        borderBottomRightRadius: 0,
        borderTopRightRadius: 0
    },
    withoutBorderLeft: {
        borderBottomLeftRadius: 0,
        borderTopLeftRadius: 0
    }
})
