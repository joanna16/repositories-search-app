const images = {
    hamburger: require('./images/hamburger.png'),
    search: require('./images/search.png'),
    male: require('./images/male.png'),
    share: require('./images/share.png'),
    star: require('./images/star.png'),
    back: require('./images/back.png'),
    shareWhite: require('./images/share_white.png'),
    arrowRight: require('./images/arrow_right.png'),
    language: require('./images/language.png'),
    owner: require('./images/owner.png'),
    issues: require('./images/issues.png')
}

export default images
