import strings from './strings'
import images from './images'
import colors from './colors'
import dimensions from './dimensions'

const R = {
    strings,
    images,
    colors,
    dimensions
}

export default R
