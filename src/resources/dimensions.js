export default {
    borderRadius: 5,
    horizontalPadding: 5,
    topBar: {
        paddingBottom: 10,
        paddingTop: 30
    },
    header: {
        height: 260,
        avatarSize: 80,
        avatarMarginBottom: 15,
        avatarBorderWidth: 3,
        marginTop: 25,
        paddingBottom: 8
    },
    cell: {
        height: 50,
        imageSize: 25
    }
}
