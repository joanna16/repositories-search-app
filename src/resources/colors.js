const colors = {
    black: '#000000',
    white: '#FFFFFF',
    darkGray: '#353535',
    mediumGray: '#C8C7CD',
    lightGray: '#EFEEF3',
    blue: '#496AB0'
}

export default colors
