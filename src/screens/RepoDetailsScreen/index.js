import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet
} from 'react-native'
import { withNavigation } from 'react-navigation'
import Header from './components/Header'
import RepoDetailsSection from './components/RepoDetailsSection'

function ReposDetailsScreen(props) {
    const { navigation } = props
    const repo = navigation.getParam('repo')

    return (
        <View style={styles.view}>
            <Header
                description={repo.description}
                avatar={repo.owner.avatar_url}
                name={repo.name}
            />
            <RepoDetailsSection repo={repo} />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1
    }
})

ReposDetailsScreen.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
        getParam: PropTypes.func.isRequired
    }).isRequired
}

export default withNavigation(ReposDetailsScreen)
