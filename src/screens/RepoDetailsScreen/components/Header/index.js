import React from 'react'
import PropTypes from 'prop-types'
import {
    View, Text, StyleSheet, Image
} from 'react-native'
import { withNavigation } from 'react-navigation'
import R from 'resources/R'
import ImageButton from 'library/components/ImageButton'

function Header(props) {
    const {
        navigation, name, description, avatar
    } = props

    return (
        <View style={styles.view}>
            <View style={styles.backButton}>
                <ImageButton
                    onPress={() => { navigation.pop() }}
                    image={R.images.back}
                />
            </View>
            <View style={styles.shareButton}>
                <ImageButton
                    onPress={() => {}}
                    image={R.images.shareWhite}
                />
            </View>
            <View style={styles.imageWrapper}>
                <Image style={styles.image} source={{ uri: avatar }} />
            </View>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.description}>{description}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: R.colors.darkGray,
        flexDirection: 'column',
        height: R.dimensions.header.height,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: R.dimensions.header.paddingBottom
    },
    imageWrapper: {
        width: R.dimensions.header.avatarSize + 2 * R.dimensions.header.avatarBorderWidth,
        height: R.dimensions.header.avatarSize + 2 * R.dimensions.header.avatarBorderWidth,
        borderRadius: R.dimensions.header.avatarSize / 2 + R.dimensions.header.avatarBorderWidth,
        backgroundColor: R.colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: R.dimensions.header.avatarMarginBottom
    },
    image: {
        width: R.dimensions.header.avatarSize,
        height: R.dimensions.header.avatarSize,
        borderRadius: R.dimensions.header.avatarSize / 2
    },
    name: {
        color: R.colors.white,
        fontWeight: 'bold',
        fontSize: 20
    },
    description: {
        color: R.colors.white,
        fontSize: 16,
        textAlign: 'center'
    },
    backButton: {
        position: 'absolute',
        left: R.dimensions.horizontalPadding,
        top: R.dimensions.header.marginTop
    },
    shareButton: {
        position: 'absolute',
        right: R.dimensions.horizontalPadding,
        top: R.dimensions.header.marginTop
    }
})

Header.defaultProps = {
    description: ''
}

Header.propTypes = {
    navigation: PropTypes.shape({
        pop: PropTypes.func.isRequired
    }).isRequired,
    avatar: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string
}

export default withNavigation(Header)
