import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet, ScrollView
} from 'react-native'
import R from 'resources/R'
import Moment from 'moment'
import SingleCell from './components/SingleCell'
import DoubleCell from './components/DoubleCell'
import TripleCell from './components/TripleCell'

function RepoDetailsSection(props) {
    const { repo } = props
    const createdAt = Moment(repo.created_at).format('DD/MM/YYYY')

    return (
        <ScrollView>
            <View style={styles.view}>
                <View style={styles.section}>
                    <TripleCell
                        firstCell={{ number: repo.stargazers_count, text: 'Stargazers' }}
                        secondCell={{ number: repo.watchers_count, text: 'Watchers' }}
                        thirdCell={{ number: repo.forks_count, text: 'Forks' }}
                    />
                </View>

                <View style={styles.section}>
                    <DoubleCell
                        borderBottom
                        firstCell={{ image: R.images.search, text: repo.private ? 'Private' : 'Public' }}
                        secondCell={{ image: R.images.language, text: repo.language }}
                    />
                    <DoubleCell
                        borderBottom
                        firstCell={{
                            image: R.images.issues,
                            text: repo.open_issues_count.toString()
                        }}
                        secondCell={{ image: R.images.search, text: '111' }}
                    />
                    <DoubleCell
                        borderBottom
                        firstCell={{ image: R.images.search, text: createdAt }}
                        secondCell={{ image: R.images.search, text: `${repo.size}MB` }}
                    />
                    <SingleCell image={R.images.owner} text="Owner" value={repo.owner.login} />
                </View>

                <View style={styles.section}>
                    <SingleCell image={R.images.search} text="Events" borderBottom />
                    <SingleCell image={R.images.search} text="Issues" borderBottom />
                    <SingleCell image={R.images.search} text="Readme" />
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: R.colors.lightGray
    },
    section: {
        marginBottom: R.dimensions.cell.height / 2
    }
})

RepoDetailsSection.propTypes = {
    repo: PropTypes.object.isRequired // TODO pass this prop prettier
}

export default RepoDetailsSection
