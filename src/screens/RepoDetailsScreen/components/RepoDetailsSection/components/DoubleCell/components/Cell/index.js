import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet, Text, Image
} from 'react-native'
import R from 'resources/R'

function Cell(props) {
    const {
        text, borderRight, image
    } = props

    return (
        <View style={[styles.view, borderRight && styles.borderRight]}>
            <Image source={image} style={styles.image} />
            <Text style={styles.text}>{text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: R.dimensions.horizontalPadding
    },
    text: {
        fontSize: 16,
        color: R.colors.darkGray,
        lineHeight: 17,
        paddingLeft: R.dimensions.horizontalPadding * 2
    },
    borderRight: {
        borderRightWidth: 1,
        borderColor: R.colors.lightGray
    },
    image: {
        height: R.dimensions.cell.imageSize,
        width: R.dimensions.cell.imageSize,
        resizeMode: 'contain'
    }
})

Cell.defaultProps = {
    borderRight: false
}

Cell.propTypes = {
    image: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    borderRight: PropTypes.bool
}

export default Cell
