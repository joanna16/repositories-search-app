import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet
} from 'react-native'
import R from 'resources/R'
import Cell from './components/Cell'

function DoubleCell(props) {
    const {
        borderBottom, firstCell, secondCell
    } = props

    return (
        <View style={[styles.view, borderBottom && styles.borderBottom]}>
            <Cell image={firstCell.image} text={firstCell.text} borderRight />
            <Cell image={secondCell.image} text={secondCell.text} />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: R.colors.white,
        height: R.dimensions.cell.height,
        flexDirection: 'row'
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderColor: R.colors.lightGray
    }
})

DoubleCell.defaultProps = {
    borderBottom: false
}

DoubleCell.propTypes = {
    borderBottom: PropTypes.bool,
    firstCell: PropTypes.shape({
        image: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired,
    secondCell: PropTypes.shape({
        image: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired
}

export default DoubleCell
