import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet, Image, Text, TouchableOpacity
} from 'react-native'
import R from 'resources/R'

function SingleCell(props) {
    const {
        image, text, value, borderBottom
    } = props

    return (
        <TouchableOpacity>
            <View style={styles.view}>
                <Image source={image} style={styles.image} />
                <View style={[styles.rowWrapper, borderBottom && styles.borderBottom]}>
                    <View style={styles.textsWrapper}>
                        <Text style={styles.text}>{text}</Text>
                        <Text style={styles.text}>{value}</Text>
                    </View>
                    <Image source={R.images.arrowRight} style={styles.arrowRight} />
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: R.colors.white,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: R.dimensions.cell.height,
        paddingRight: R.dimensions.horizontalPadding,
        paddingLeft: R.dimensions.horizontalPadding
    },
    image: {
        height: R.dimensions.cell.imageSize,
        width: R.dimensions.cell.imageSize,
        resizeMode: 'contain'
    },
    rowWrapper: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        height: '100%'
    },
    textsWrapper: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        paddingRight: R.dimensions.horizontalPadding * 2,
        paddingLeft: R.dimensions.horizontalPadding * 2
    },
    text: {
        fontSize: 18,
        color: R.colors.darkGray
    },
    arrowRight: {
        marginTop: 3
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderColor: R.colors.lightGray
    }
})

SingleCell.defaultProps = {
    value: '',
    borderBottom: false
}

SingleCell.propTypes = {
    image: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    value: PropTypes.string,
    borderBottom: PropTypes.bool
}

export default SingleCell
