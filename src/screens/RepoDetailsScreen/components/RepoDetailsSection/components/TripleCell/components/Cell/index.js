import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet, Text
} from 'react-native'
import R from 'resources/R'

function Cell(props) {
    const {
        number, text, borderRight
    } = props

    return (
        <View style={[styles.view, borderRight && styles.borderRight]}>
            <Text style={[styles.text, styles.bold]}>{number}</Text>
            <Text style={styles.text}>{text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 16,
        color: R.colors.darkGray,
        lineHeight: 17
    },
    bold: {
        fontWeight: 'bold'
    },
    borderRight: {
        borderRightWidth: 1,
        borderColor: R.colors.lightGray
    }
})

Cell.defaultProps = {
    borderRight: false
}

Cell.propTypes = {
    number: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    borderRight: PropTypes.bool
}

export default Cell
