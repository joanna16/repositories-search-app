import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet
} from 'react-native'
import R from 'resources/R'
import Cell from './components/Cell'

function TripleCell(props) {
    const {
        firstCell, secondCell, thirdCell
    } = props

    return (
        <View style={styles.view}>
            <Cell number={firstCell.number} text={firstCell.text} borderRight />
            <Cell number={secondCell.number} text={secondCell.text} borderRight />
            <Cell number={thirdCell.number} text={thirdCell.text} />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: R.colors.white,
        height: R.dimensions.cell.height,
        flexDirection: 'row'
    }
})

TripleCell.defaultProps = {
}

TripleCell.propTypes = {
    firstCell: PropTypes.shape({
        number: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired,
    secondCell: PropTypes.shape({
        number: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired,
    thirdCell: PropTypes.shape({
        number: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired
}

export default TripleCell
