import { fromJS } from 'immutable'
import { SEGMENTED_CONTROL_STATE, REPOS_LIST, REPOS_SEARCH_BAR_VALUE } from './actions'
import { REPOSITORIES } from './constants'

const initialState = fromJS({
    segmentedControlValue: REPOSITORIES,
    reposList: [],
    reposSearchBarValue: ''
})

const reducer = (state = initialState, action) => {
    switch (action.type) {
    case SEGMENTED_CONTROL_STATE:
        return state.set('segmentedControlValue', action.state)
    case REPOS_LIST:
        return state.set('reposList', action.state)
    case REPOS_SEARCH_BAR_VALUE:
        return state.set('reposSearchBarValue', action.state)
    default:
        return state
    }
}

export default reducer
