export function getSegmentedControlState(state) {
    return state.RepoListScreenReducer.get('segmentedControlValue')
}

export function getReposList(state) {
    return state.RepoListScreenReducer.get('reposList')
}

export function getReposSearchBarValue(state) {
    return state.RepoListScreenReducer.get('reposSearchBarValue')
}
