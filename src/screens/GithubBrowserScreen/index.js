import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getSegmentedControlState } from 'screens/GithubBrowserScreen/selectors'
import { REPOSITORIES, USERS } from 'screens/GithubBrowserScreen/constants'
import { StyleSheet, View } from 'react-native'
import R from 'resources/R'
import TopBar from './components/TopBar'
import ReposList from './components/ReposList'
import UsersList from './components/UsersList'

function GithubBrowserScreen(props) {
    const { _getSegmentedControlState } = props
    return (
        <View style={styles.view}>
            <TopBar />
            <View style={styles.listView}>
                {_getSegmentedControlState === REPOSITORIES ? (<ReposList />) : (<UsersList />)}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: R.colors.white,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    listView: {
        alignSelf: 'stretch',
        flex: 1
    }
})

GithubBrowserScreen.propTypes = {
    _getSegmentedControlState: PropTypes.oneOf([REPOSITORIES, USERS]).isRequired
}

const mapStateToProps = state => ({
    _getSegmentedControlState: getSegmentedControlState(state)
})

export default connect(mapStateToProps)(GithubBrowserScreen)
