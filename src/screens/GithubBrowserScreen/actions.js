export const SEGMENTED_CONTROL_STATE = 'SEGMENTED_CONTROL_STATE'
export const REPOS_LIST = 'REPOS_LIST'
export const REPOS_SEARCH_BAR_VALUE = 'REPOS_SEARCH_BAR_VALUE'

export function setSegmentedControlState(state) {
    return { type: SEGMENTED_CONTROL_STATE, state }
}

export function setReposList(state) {
    return { type: REPOS_LIST, state }
}

export function setReposSearchBarValue(state) {
    return { type: REPOS_SEARCH_BAR_VALUE, state }
}
