import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet, View, TextInput, Image
} from 'react-native'
import R from 'resources/R'

class SearchBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            value: ''
        }
    }

    updateValue = (updatedValue) => {
        this.setState({ value: updatedValue })
    }

    render() {
        const { onPress } = this.props
        const { value } = this.state

        return (
            <View style={styles.view}>
                <TextInput
                    style={styles.textInput}
                    placeholder="Type something"
                    returnKeyType="search"
                    value={value}
                    onChangeText={updatedValue => this.updateValue(updatedValue)}
                    onSubmitEditing={() => onPress(value)}
                    clearButtonMode="while-editing"
                />
                <Image style={styles.image} source={R.images.search} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 0,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 8,
        paddingRight: 8,
        alignSelf: 'stretch',
        justifyContent: 'center',
        backgroundColor: R.colors.mediumGray
    },
    image: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 15
    },
    textInput: {
        flex: 0,
        padding: 8,
        paddingLeft: 35,
        backgroundColor: R.colors.white,
        color: R.colors.darkGray,
        borderRadius: R.dimensions.borderRadius,
        fontSize: 18
    }
})

SearchBar.propTypes = {
    onPress: PropTypes.func.isRequired
}

export default SearchBar
