import React from 'react'
import PropTypes from 'prop-types'
import {
    Text, TouchableOpacity, View, Image, StyleSheet
} from 'react-native'
import R from 'resources/R'
import TextWithImage from './components/TextWithImage'

export default function ListItem(props) {
    const {
        onPress, name, description, avatar, owner, forksCount, score
    } = props

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.6}
        >
            <View style={styles.listItemWrapper}>
                <View style={styles.imageWrapper}>
                    <Image style={styles.image} source={{ uri: avatar }} />
                </View>
                <View style={styles.infoWrapper}>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.description}>{description}</Text>
                    <View style={styles.additionalInfo}>
                        <TextWithImage text={score} image={R.images.star} />
                        <TextWithImage text={forksCount} image={R.images.share} />
                        <TextWithImage text={owner} image={R.images.male} />
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

ListItem.defaultProps = {
    description: ''
}

ListItem.propTypes = {
    onPress: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    avatar: PropTypes.string.isRequired,
    owner: PropTypes.string.isRequired,
    forksCount: PropTypes.number.isRequired,
    score: PropTypes.number.isRequired
}

const styles = StyleSheet.create({
    listItemWrapper: {
        flexDirection: 'row',
        paddingTop: 8
    },
    imageWrapper: {
        paddingLeft: 8,
        paddingRight: 8
    },
    image: {
        borderRadius: 25,
        width: 50,
        height: 50,
        marginTop: 5
    },
    infoWrapper: {
        paddingRight: 10,
        justifyContent: 'flex-start',
        flex: 1,
        flexWrap: 'wrap',
        borderBottomWidth: 1,
        borderColor: R.colors.mediumGray
    },
    name: {
        color: R.colors.blue,
        fontSize: 18,
        fontWeight: 'bold'
    },
    description: {
        color: R.colors.darkGray,
        fontSize: 16
    },
    additionalInfo: {
        flexDirection: 'row',
        color: R.colors.mediumGray,
        paddingBottom: 8,
        paddingTop: 8
    }
})
