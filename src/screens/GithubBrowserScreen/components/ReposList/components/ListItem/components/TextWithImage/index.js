import React from 'react'
import PropTypes from 'prop-types'
import {
    View, StyleSheet, Image, Text
} from 'react-native'
import R from 'resources/R'

export default function TextWithImage(props) {
    const { image, text } = props

    return (
        <View style={styles.view}>
            <Image source={image} style={styles.image} />
            <Text style={styles.text}>{text}</Text>
        </View>
    )
}

TextWithImage.propTypes = {
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    image: PropTypes.number.isRequired
}

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 5
    },
    text: {
        fontSize: 14,
        color: R.colors.mediumGray
    },
    image: {
        height: 20,
        resizeMode: 'contain'
    }
})
