import React, { Component } from 'react'
import { ActivityIndicator, StyleSheet, View, FlatList, Dimensions } from 'react-native'
import SearchBar from './components/SearchBar'
import ListItem from './components/ListItem'
import { connect } from 'react-redux'
import { setReposList } from 'screens/GithubBrowserScreen/actions'
import { getReposList } from 'screens/GithubBrowserScreen/selectors'
import PropTypes from 'prop-types'
import R from 'resources/R'
import { withNavigation } from 'react-navigation'

class ReposList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            loading: false,
            error: null
        }
    }

    findRepositories = (value) => {
        const url = 'https://api.github.com/search/repositories?q=' + value
        const { _setReposList } = this.props
        this.setState({ loading: true })
        fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    error: res.error || null,
                    loading: false
                });
                _setReposList(res.items)
            })
            .catch(error => {
                this.setState({ error, loading: false })
                console.log(error)
            })
    }

    goToRepoDetails = (repo) => {
        const { navigation } = this.props
        navigation.navigate('RepoDetailsScreen', {
            repo: repo
        })
    }

    render() {
        const { _reposList } = this.props
        const { loading } = this.state
        return (
            <View style={styles.view}>
                <SearchBar onPress={value => this.findRepositories(value)}/>
                <FlatList
                    data={_reposList}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) =>
                        <ListItem
                            onPress={() => this.goToRepoDetails(item)}
                            name={item.name}
                            description={item.description}
                            avatar={item.owner.avatar_url}
                            owner={item.owner.login}
                            forksCount={item.forks_count}
                            score={item.score}
                        />
                    }
                    style={styles.flatList}
                />
                {
                    loading &&
                    <View style={styles.activityIndicatorView}>
                        <ActivityIndicator
                            color={R.colors.blue}
                            size="large"
                        />
                    </View>
                }
            </View>
        )
    }
}

ReposList.propTypes = {
    _reposList: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
    _setReposList: PropTypes.func.isRequired,
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired
    }).isRequired

}

const styles = StyleSheet.create({
    activityIndicatorView: {
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        // TODO: fix flat list height and remove the line below
        height: Dimensions.get('window').height
    },
    view: {
        paddingBottom: 50
    }
})

const mapStateToProps = state => ({
    _reposList: getReposList(state)
})

const mapDispatchToProps = dispatch => ({
    _setReposList: state => dispatch(setReposList(state))
})

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(ReposList))
