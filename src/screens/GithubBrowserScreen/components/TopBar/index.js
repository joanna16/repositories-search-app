import React from 'react'
import { StyleSheet, View } from 'react-native'
import ImageButton from 'library/components/ImageButton'
import R from 'resources/R'
import SegmentedControl from './components/SegmentedControl'

export default function TopBar() {
    return (
        <View style={styles.view}>
            <View style={styles.alignedLeft}>
                <ImageButton onPress={() => {}} image={R.images.hamburger} />
            </View>
            <SegmentedControl />
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 0,
        backgroundColor: R.colors.darkGray,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingTop: R.dimensions.topBar.paddingTop,
        paddingBottom: R.dimensions.topBar.paddingBottom,
        flexDirection: 'row',
        position: 'relative'
    },
    alignedLeft: {
        position: 'absolute',
        left: R.dimensions.horizontalPadding,
        bottom: R.dimensions.topBar.paddingBottom
    }
})
