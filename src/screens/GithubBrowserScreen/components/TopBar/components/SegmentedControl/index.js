import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { setSegmentedControlState } from 'screens/GithubBrowserScreen/actions'
import { REPOSITORIES, USERS } from 'screens/GithubBrowserScreen/constants'
import { getSegmentedControlState } from 'screens/GithubBrowserScreen/selectors'
import strings from 'resources/strings'
import OutlineButton from 'library/components/OutlineButton'

function SegmentedControl(props) {
    const { _getSegmentedControlState, _setSegmentedControlState } = props

    return (
        <View style={styles.view}>
            <OutlineButton
                onPress={() => _setSegmentedControlState(REPOSITORIES)}
                active={_getSegmentedControlState === REPOSITORIES}
                withoutBorderRight
            >
                {strings.repositories}
            </OutlineButton>

            <OutlineButton
                onPress={() => _setSegmentedControlState(USERS)}
                active={_getSegmentedControlState === USERS}
                withoutBorderLeft
            >
                {strings.users}
            </OutlineButton>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 0,
        flexDirection: 'row'
    }
})


SegmentedControl.propTypes = {
    _getSegmentedControlState: PropTypes.oneOf([REPOSITORIES, USERS]).isRequired,
    _setSegmentedControlState: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    _getSegmentedControlState: getSegmentedControlState(state)
})

const mapDispatchToProps = dispatch => ({
    _setSegmentedControlState: state => dispatch(setSegmentedControlState(state))
})

export default connect(mapStateToProps, mapDispatchToProps)(SegmentedControl)
