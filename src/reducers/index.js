import { combineReducers } from 'redux'
import RepoListScreenReducer from 'screens/GithubBrowserScreen/reducers'

const reducers = combineReducers({
    RepoListScreenReducer
})

export default reducers
