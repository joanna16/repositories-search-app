import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import GithubBrowserScreen from 'screens/GithubBrowserScreen'
import RepoDetailsScreen from 'screens/RepoDetailsScreen'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import reducers from './src/reducers'

const store = createStore(reducers)

const stackNavigator = createStackNavigator({
    GithubBrowserScreen,
    RepoDetailsScreen
},
{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false
    }
})

const Navigation = createAppContainer(stackNavigator)

export default function App() {
    return (
        <Provider store={store}>
            <Navigation />
        </Provider>
    )
}
